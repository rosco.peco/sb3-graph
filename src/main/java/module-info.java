module com.xdesign.rossbamford.sbthree {

    requires java.sql;
    requires lombok;
    requires org.neo4j.cypherdsl.core;
    requires org.slf4j;
    requires reactor.core;
    requires spring.beans;
    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires spring.context;
    requires spring.core;
    requires spring.tx;
    requires spring.data.commons;
    requires spring.data.neo4j;
    requires spring.data.rest.core;
    requires spring.web;
    requires spring.webflux;
    requires spring.graphql;
    requires org.neo4j.driver;

    opens com.xdesign.rossbamford.sb3.config;
    opens com.xdesign.rossbamford.sb3.controller;
    opens com.xdesign.rossbamford.sb3.model;
    opens com.xdesign.rossbamford.sb3 to spring.core;

    exports com.xdesign.rossbamford.sb3;
}