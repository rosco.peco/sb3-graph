package com.xdesign.rossbamford.sb3.model;

import lombok.Builder;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;

@Node
@Builder(toBuilder = true)
public record Person(
        @Id @GeneratedValue Long id,
        @Property("name") String name,
        @Property("born") Integer bornYear
) { }
