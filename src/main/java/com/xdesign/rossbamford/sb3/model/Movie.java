package com.xdesign.rossbamford.sb3.model;

import lombok.Builder;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;
import org.springframework.data.neo4j.core.schema.Relationship;

import java.util.Set;

@Node
@Builder(toBuilder = true)
public record Movie(
        @Id @GeneratedValue Long id,
        @Property("title") String title,
        @Property("tagline") String description,
        @Property("released") Integer releaseYear,
        @Relationship(type = "WROTE", direction = Relationship.Direction.INCOMING) Set<Person> writers,
        @Relationship(type = "DIRECTED", direction = Relationship.Direction.INCOMING) Set<Person> directors,
        @Relationship(type = "PRODUCED", direction = Relationship.Direction.INCOMING) Set<Person> producers,
        @Relationship(type = "ACTED_IN", direction = Relationship.Direction.INCOMING) Set<ActorRoles> actors
) { }
