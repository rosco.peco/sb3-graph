package com.xdesign.rossbamford.sb3.model;

import lombok.Builder;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.RelationshipProperties;
import org.springframework.data.neo4j.core.schema.TargetNode;

import java.util.Set;

@Builder(toBuilder = true)
@RelationshipProperties
public record ActorRoles(
        @Id @GeneratedValue Long id,
        Set<String> roles,
        @TargetNode Person actor
) { }
