package com.xdesign.rossbamford.sb3.repository.reactive;

import com.xdesign.rossbamford.sb3.model.Movie;
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface ReactiveMovieRepository extends ReactiveNeo4jRepository<Movie, Long> {
    Mono<Movie> findOneById(Long id);
    Flux<Movie> findByTitle(String title);
}
