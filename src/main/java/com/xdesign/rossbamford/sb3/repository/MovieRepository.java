package com.xdesign.rossbamford.sb3.repository;

import com.xdesign.rossbamford.sb3.model.Movie;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends Neo4jRepository<Movie, Long> {
}
