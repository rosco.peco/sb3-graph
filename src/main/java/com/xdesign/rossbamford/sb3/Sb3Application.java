package com.xdesign.rossbamford.sb3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EnableNeo4jRepositories(basePackageClasses = com.xdesign.rossbamford.sb3.repository.PackageMarker.class)
public class Sb3Application {

    public static void main(String[] args) {
        SpringApplication.run(Sb3Application.class, args);
    }
}
