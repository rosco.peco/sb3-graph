package com.xdesign.rossbamford.sb3.controller;

import com.xdesign.rossbamford.sb3.model.Movie;
import com.xdesign.rossbamford.sb3.repository.reactive.ReactiveMovieRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
@RequiredArgsConstructor
public class MovieGraphQLController {
    private final ReactiveMovieRepository repository;

    @QueryMapping
    public Flux<Movie> movies() {
        return repository.findAll();
    }

    @QueryMapping
    public Mono<Movie> movieById(@Argument final Long id) {
        return repository.findOneById(id);
    }

    @QueryMapping
    public Flux<Movie> moviesByTitle(@Argument final String title) {
        return repository.findByTitle(title);
    }
}
