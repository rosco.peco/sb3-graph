FROM --platform=linux/amd64 eclipse-temurin:19-alpine
EXPOSE 8080
RUN mkdir app
WORKDIR app
COPY scripts/docker-run.sh .
COPY build/libs/sb3*.jar .
RUN rm -f *plain*.jar
ENTRYPOINT ["sh", "/app/docker-run.sh"]
ENV LANG C.UTF-8
ENV JAVA_HOME=/opt/java/openjdk
